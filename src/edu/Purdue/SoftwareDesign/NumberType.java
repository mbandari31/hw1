package edu.Purdue.SoftwareDesign;
/**
 * 
 * @author Mounika
 * This class is used to set the number type of the numbers like integer.
 * It consists of number1 and number2 getters and setters.
 */
public abstract class NumberType{
/**
 * The variable number1 is the first number input by the user. 
 */
	private NumberType number1;
	/**
	 * The variable number2 is the second input by the user.
	 */
	private NumberType number2;
	
	/**
	 * getNumber1 is used to get the value of variable number1.
	 * @return number1.
	 */
	public NumberType getNumber1() {
		return number1;
	}
	/**
	 * setNumber1 is used to set the value of variable number1.
	 * @param number1
	 */
	public void setNumber1(NumberType number1) {
		this.number1 = number1;
	}
	/**
	 * getNumber2 is used to get the value of variable number2.
	 * @return number2 value.
	 */
	public NumberType getNumber2() {
		return number2;
	}
/**
 * setNumber2 is used to set the value of variable number2.
 * @param number2
 */
	public void setNumber2(NumberType number2) {
		this.number2 = number2;
	}
	/**
	 * This method is used to set the input strings to integers
	 * @param num1 the first input from the user.
	 * @param num2 the second input from the user.
	 */
	public void setValues(String num1,String num2){
		
	}
	/**
	 * getNo1 is used to get the value of the IntegerNumber number1.
	 * @return IntegerValue number1
	 */
	public int getNo1() {
		return 9;
	}
		/**
		 * getNo1 is used to get the value of the IntegerNumber number2.
		 * @return IntegerValue number2
		 */
	public int getNo2() {
		return 0;
	}
}

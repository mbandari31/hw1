package edu.Purdue.SoftwareDesign;
/**
 * 
 * @author Mounika
 *
 */
public class IntegerType extends NumberType{
	/**
	 * It is the first integer value for calculation. 
	 */
	private static int no1;
	/**
	 * It is the second integer value for calculation.
	 */
	private static int no2;
	
	/**
	 * @param no11 The string input from the user is passed.
	 * @param no12 The string input from the user is passed.
	 */
	public IntegerType(String no11, String no12) {
		setValues(no11,no12);
	}

	/**
	 * @param no11 This param is the first string given by user.
	 * @param no12 This param is the second string given by user.
	 */
	@Override
	public void setValues(String no11, String no12) {
		setNo1(Integer.parseInt(no11));
		setNo2(Integer.parseInt(no12));
	}
	
	/**
	 * @return int The Integer no1 is return value.
	 */
	public int getNo1() {
		// TODO Auto-generated method stub
		return this.no1;
	}
/**
 * @return int The Integer no2 is return value.
 */
	
	public int getNo2() {
		// TODO Auto-generated method stub
		return this.no2;
	}

	/**
	 * This value is used to set the integer value to the no1. 
	 * @param num1 It is string converted to integer.
	 */
	public void setNo1(int num1) {
		// TODO Auto-generated method stub
		this.no1 = num1;
	}
	/**
	 * This value is used to set the integer value to the no2. 
	 * @param num2 It is string converted to integer.
	 */
	public void setNo2(int num2) {
		// TODO Auto-generated method stub
		this.no2 = num2;
	}
}

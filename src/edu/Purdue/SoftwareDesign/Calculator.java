package edu.Purdue.SoftwareDesign;

public class Calculator {
	private NumberType numType;
	private Operation operat;
	
	public void setNumberType(NumberType numType1){
		this.numType = numType1;
	}
	
	public void setOperation(Operation operat){
		this.operat = operat;
	}
	
	public void Calculate() {
		operat.Calculate(this.numType);
	}
}

package edu.Purdue.SoftwareDesign;
/**
 * 
 * @author Mounika
 *
 */
public class Subtraction implements Operation{

	public Subtraction(){
		
	}
	/**
	 * Calculate method in Interface Operation according to Subtraction application.
	 */
	@Override
	public void Calculate(NumberType nums)
	{
		System.out.println(nums.getNo1() - nums.getNo2());
	}
}

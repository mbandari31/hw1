package edu.Purdue.SoftwareDesign;

/**
 * @author Mounika
 *
 */
public class Division implements Operation{

	public Division(){
		
	}
	
	/**
	 * Calculate method in Interface Operation according to Division application.
	 */
	@Override
	public void Calculate(NumberType nums)
	{
		System.out.println(nums.getNo1() / nums.getNo2());
	}
}

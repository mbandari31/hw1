package edu.Purdue.SoftwareDesign;

import java.util.Scanner;

/**
 * @author Mounika
 *
 */
public class Test {
	
	public static void main(String[] args) {	
		/**
		 * opr It is used to take the which input operation user wants. 
		 */
		Operation opr = null;
		/**
		 * cal has Operation and numberType. 
		 */
		Calculator cal = new Calculator();
		/**
		 * numTypeObj is used to select whether the number is Integer or decimal.
		 */
		NumberType numTypeObj = null;
		/**
		 * operation is the input number from user to perform operation. 
		 */
		int operation = 0;
		System.out.println("Enter two nos");
		Scanner scanner = new Scanner(System.in);
		String no1 = scanner.nextLine();
		String no2 = scanner.nextLine();
		
		System.out.println("Select a number to calculate : \n" +
				"1. Addition \n" +
				"2. Subtraction \n" +
				"3. Multiplication \n" +
				"4. Division \n");
		
		operation = Integer.parseInt(scanner.nextLine());
		if((operation == 1) || (operation == 2) || (operation == 3) || (operation == 4) )
		{
			switch(operation){
			case 1: opr = new Addition();
			break;
			
			case 2: opr = new Subtraction();
			break;
			
			case 3: opr = new Multiplication();
			break;
			
			case 4: opr = new Division();
			break;
			
			}
			cal.setOperation(opr);
			
			if(IsInterger(no1,no2))
			{
				numTypeObj = new IntegerType(no1, no2);
				
			}
			else
			{
				// double type
			}
			
			cal.setNumberType(numTypeObj);
			
			cal.Calculate();
			
		}
		else
		{
			System.out.println("select from above");
		}
	}
	
	/**
	 * This method checks whether the given numbers from the user are integers or decimals
	 * @param no1 The input no1 from the user is taken in form of string
	 * @param no2 The input no2 from the user is taken in form of string
	 * @return It returns a boolean value if the values are integers else it returns false.
	 */
	public static Boolean IsInterger(String no1,String no2)
	{ 	boolean isInt = false;
		if(no1.contains(".") || no2.contains(".") )
		{
			isInt = false;			
		}
		else
		{
			isInt = true;
		}
		return isInt;
	}
}

package edu.Purdue.SoftwareDesign;

/**
 * @author Mounika
 *
 */
public class Addition implements Operation{
	public Addition() {
		
	}
	/**
	 * Calculate method in Interface Operation according to Addition application.
	 */
	@Override
	public void Calculate(NumberType nums)
	{
		System.out.println(nums.getNo1() + nums.getNo2());
	}
}

package edu.Purdue.SoftwareDesign;

public interface Operation{
	/**
	 * 
	 * @param numbers To get the number1 and number2 for calculation
	 */
	public void Calculate(NumberType numbers);
	
}
